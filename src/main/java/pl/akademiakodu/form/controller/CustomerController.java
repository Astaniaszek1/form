package pl.akademiakodu.form.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.akademiakodu.form.dao.CustomerDao;
import pl.akademiakodu.form.dao.MessageCustomerDao;
import pl.akademiakodu.form.model.Customer;
import pl.akademiakodu.form.model.MessageCustomer;

import java.util.ArrayList;
import java.util.List;

@Controller
public class CustomerController {


    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private MessageCustomerDao messageCustomerDao;


    @GetMapping("/")
    public String start() {
        return "start";
    }


    @PostMapping("/addcustomer")
    public String add(@ModelAttribute Customer customer, ModelMap modelMap) {
        customerDao.save(customer);
        modelMap.put("customer", customer);
        return "message";
    }


    @PostMapping("/addcustomer/addmessage")
    public String messageCust(@ModelAttribute MessageCustomer messageCustomer) {
        messageCustomerDao.save(messageCustomer);
        return "end";
    }

    @GetMapping("/database")
    public String base(ModelMap modelMap) {
        modelMap.put("customers", customerDao.findAll());
        return "database";
    }

    @Autowired
    CustomerDao customer;

    @GetMapping("/search")
    public String list(@RequestParam("surname1") String sur, ModelMap modelMap) {
        List<Customer> customers = customer.findBySurname(sur);
        System.out.println("WYNIK"+customers.size());
        if ( !customers.isEmpty()){
            modelMap.put("message","Znalezione osoby o nazwisku "+sur+" to:");
        }
        else{
            modelMap.put("message","Nie znaleziono osoby o podanym nazwisku");
        }
        modelMap.put("customers",customers);
        return "database";
    }
}