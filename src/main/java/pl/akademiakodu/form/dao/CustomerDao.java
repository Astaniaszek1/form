package pl.akademiakodu.form.dao;

import org.springframework.data.repository.CrudRepository;
import pl.akademiakodu.form.model.Customer;

import java.util.List;

public interface CustomerDao extends CrudRepository<Customer, Long> {
    List<Customer> findBySurname(String surname);
}
