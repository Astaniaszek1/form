package pl.akademiakodu.form.dao;

import org.springframework.data.repository.CrudRepository;
import pl.akademiakodu.form.model.Customer;
import pl.akademiakodu.form.model.MessageCustomer;

public interface MessageCustomerDao extends CrudRepository<MessageCustomer, Long> {
}
